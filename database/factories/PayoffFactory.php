<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Payoff>
 */
class PayoffFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $accountNumber = $this->faker->randomNumber(9);
        $bankCode = $this->faker->randomNumber(4);

        return [
            'id' => $this->faker->unique()->randomNumber(),
            'bankAccountNumber' => sprintf('%010d/%04d', $accountNumber, $bankCode),
            'amount' => rand(-100, 100),
        ];
    }
}
