<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ApiTokenMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next)
    {
        $token = Config::get('api.token');

        $requestToken = $request->header('Authorization');

        if (!$requestToken) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $requestToken = substr($requestToken, 7);

        if ($requestToken !== $token) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        return $next($request);
    }

}
