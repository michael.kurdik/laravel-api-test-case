<?php

use App\Http\Controllers\CommissionController;
use App\Http\Middleware\ApiTokenMiddleware;
use Illuminate\Support\Facades\Route;

Route::middleware(ApiTokenMiddleware::class)
    ->prefix('v1')->group(function () {
        Route::get('commission/available', [CommissionController::class, 'available']);
        Route::post('commission/withdraw', [CommissionController::class, 'withdraw']);
    });

