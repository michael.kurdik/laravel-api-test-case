# Zadání

Vytvořte API službu za využití frameworku Laravel ve verzi 11.x. Projekt bude obsahovat dva modely:
- `Commission` reprezentující provizi, která bude obsahovat částku. Částka může být záporná (poplatek).
- `Payoff` reprezentující žádost o vyplacení, která bude obsahovat číslo bankovního účtu.

Služba bude poskytovat dva endpointy:

`/api/v1/commission/available` \
Vypíše všechny kladné dostupné kombinace částek k vyplacení seřazených vzestupně. Při vyplacení musí být zúčtovány všechny poplatky.
> Příklad: Existují 3 dostupné provize (poplatky) k vyplacení -10, 5 a 15. Dostupné kombinace tedy jsou 5 a 10.

\
`/api/v1/commission/withdraw` \
Vytvoří žádost o vyplacení zvolené částky na požadovaný bankovní účet. O vyplacení lze žádat z každé IP adresy pouze jednou za minutu. Každou provizi lze vyplatit pouze jednou.


# Manual

**Dokerizace**

Mám rád čistý lokální stroj, proto docker. Zároveň jsem nechtěl jeho konfiguraci dělat složitější než je nutné, proto je aplikace dostupná za `/public`.

\
`docker build -t php-apache-container .`

\
`docker run -it --rm -p 8080:80 -v $(pwd):/var/www/html --name php-apache-container php-apache-container`

\
`docker exec -it php-apache-container bash`

\
`curl -sS https://getcomposer.org/installer | php`

\
`mv composer.phar /usr/local/bin/composer`

\
`apt update && apt install zip && apt install unzip`

\
`composer install`

\ 
**Databáze**

---

Nechtěl jsem řešit napojení, container, atd. pro SQL DB, proto jsem zvolil cestu JSON souborů jako jednoduchý případ DB.

`php artisan db:seed`

\
**Příklad testu API**


*GET /api/v1/commission/available*

```
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://localhost:8080/public/api/v1/commission/available',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'GET',
  CURLOPT_HTTPHEADER => array(
    'Authorization: Bearer 123456789',
    'Cookie: XSRF-TOKEN=eyJpdiI6IlRhZDR6MDMrTFpmN1Z3SzgrWFUvUkE9PSIsInZhbHVlIjoidlAxOU0zMVRpdW9JU2FrNzM3bGlxSTJTdEdDR3RKNnNRZ21jN2pHWFErTjB4L3hPNjkyYk1EUjliZzVKOS9VcjFnR2FqVmNzOGExMTJra2ZJRnRyemxYdmhqOTB5b2M2RENpSWttZ3B5b2c4SVRVT09WWHllN2l3alYxbDgxZEkiLCJtYWMiOiJjNzc0YWExZTU3YjMwNTZlN2ZmZWYyMTUwYTc3YzE4ZTE2MDBiMzg5ZjA3MWMxNTU1MTM4ZWYwOGYwZWM2YjBlIiwidGFnIjoiIn0%3D; _nss=1; laravel_session=eyJpdiI6IkZOKzA0L2NFUWczemJVTG9sZmZBMlE9PSIsInZhbHVlIjoiUEJMRjVrZUtmaVd0Rk5QRWQyUlEvUTlUczJIc09GOU5oYzVlYkxkVUFTdTBpVzNpS0Zpb0xrRDV1STNyT2c3QTdHSTQyZ09xTFZoWlVZdGRDNHBiR1U2dWZlRVRldFp2Vlc0eFlrWDI5SHdYb2gvazJtOHFuWHJCcWlub3hML2IiLCJtYWMiOiIxYmZkMjBiMjViYmM2OThhMDhiZjE1NmUwNDljOGJiMDJhNzYzMzNhNGUzZGQ3Mzk5Zjk3MDgwYWM1M2RlY2U5IiwidGFnIjoiIn0%3D; panelx_enp_pl=81b32dd38602c23391d7d3d3d6452544; tracy-session=e3e8a1532a'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;
```

---

*POST /api/v1/commission/withdraw*


```
<?php

$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => 'http://localhost:8080/public/api/v1/commission/withdraw',
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => '',
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 0,
  CURLOPT_FOLLOWLOCATION => true,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => 'POST',
  CURLOPT_POSTFIELDS => 'accountId=30&commissionId=2258893&amount=59&bankAccountNumber=123232%2F12212',
  CURLOPT_HTTPHEADER => array(
    'Content-Type: application/x-www-form-urlencoded',
    'Authorization: Bearer 123456789',
    'Cookie: XSRF-TOKEN=eyJpdiI6IlRhZDR6MDMrTFpmN1Z3SzgrWFUvUkE9PSIsInZhbHVlIjoidlAxOU0zMVRpdW9JU2FrNzM3bGlxSTJTdEdDR3RKNnNRZ21jN2pHWFErTjB4L3hPNjkyYk1EUjliZzVKOS9VcjFnR2FqVmNzOGExMTJra2ZJRnRyemxYdmhqOTB5b2M2RENpSWttZ3B5b2c4SVRVT09WWHllN2l3alYxbDgxZEkiLCJtYWMiOiJjNzc0YWExZTU3YjMwNTZlN2ZmZWYyMTUwYTc3YzE4ZTE2MDBiMzg5ZjA3MWMxNTU1MTM4ZWYwOGYwZWM2YjBlIiwidGFnIjoiIn0%3D; _nss=1; laravel_session=eyJpdiI6IkZOKzA0L2NFUWczemJVTG9sZmZBMlE9PSIsInZhbHVlIjoiUEJMRjVrZUtmaVd0Rk5QRWQyUlEvUTlUczJIc09GOU5oYzVlYkxkVUFTdTBpVzNpS0Zpb0xrRDV1STNyT2c3QTdHSTQyZ09xTFZoWlVZdGRDNHBiR1U2dWZlRVRldFp2Vlc0eFlrWDI5SHdYb2gvazJtOHFuWHJCcWlub3hML2IiLCJtYWMiOiIxYmZkMjBiMjViYmM2OThhMDhiZjE1NmUwNDljOGJiMDJhNzYzMzNhNGUzZGQ3Mzk5Zjk3MDgwYWM1M2RlY2U5IiwidGFnIjoiIn0%3D; panelx_enp_pl=81b32dd38602c23391d7d3d3d6452544; tracy-session=e3e8a1532a'
  ),
));

$response = curl_exec($curl);

curl_close($curl);
echo $response;

```
