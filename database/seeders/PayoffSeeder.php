<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Commission;
use App\Models\Payoff;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class PayoffSeeder extends Seeder
{
    /**
     * Run the Payoff database seeds as JSON file.
     */
    public function run(): void
    {
        $accounts = Account::all();
        $commissions = Commission::all()->pluck('id', 'accountId');

        $dataSourcePath = Config::get('app.data_source.payoffs');

        unlink($dataSourcePath);

        $payoffs = [];

        foreach ($accounts as $account) {
            if (empty($commissions[$account->id])) {
                continue;
            }

            $payoffs = array_merge_recursive(
                $payoffs,
                Payoff::factory()->count(rand(1, 3))->make([
                   'accountId' => $account->id,
                   'commissionId' => $commissions[$account->id],
                ])->toArray()
            );
        }

        file_put_contents($dataSourcePath, json_encode($payoffs, JSON_PRETTY_PRINT));
    }
}
