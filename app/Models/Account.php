<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Config;

class Account
{
    use HasFactory;

    public ?int $id;

    public ?string $name;

    public ?string $email;

    public function __construct(array $attributes = [])
    {
        $this->id = $attributes['id'] ?? null;
        $this->name = $attributes['name'] ?? null;
        $this->email = $attributes['email'] ?? null;
    }

    public static function all(): Collection
    {
        $dataSourcePath = Config::get('app.data_source.accounts');

        if (!file_exists($dataSourcePath)) {
            return (new self())->newCollection();
        }

        $data = file_get_contents($dataSourcePath);
        $data = json_decode($data, true);
        // todo: check of data consistency?
        $data = array_map(fn(array $row) => new self($row), $data);

        return (new self())->newCollection($data);
    }

    public function newCollection(array $models = []): Collection
    {
        return new Collection($models);
    }

    public function toArray(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
        ];
    }
}
