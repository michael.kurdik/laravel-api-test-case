<?php

use Illuminate\Support\Facades\Route;
use League\CommonMark\CommonMarkConverter;

Route::get('/', function () {

    $markdownText = file_get_contents(base_path() . '/readme.md');
    $converter = new CommonMarkConverter();
    $htmlText = $converter->convertToHtml($markdownText);

    return sprintf('<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Testing commission API application</title>
</head>
<body style="padding: 3rem">
%s
</body>
</html>', $htmlText);
});
