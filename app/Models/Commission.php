<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Config;

class Commission
{
    use HasFactory;

    public ?int $id;

    public ?int $accountId;

    public ?float $amount;

    public function __construct(array $attributes = [])
    {
        $this->id = $attributes['id'] ?? null;
        $this->accountId = $attributes['accountId'] ?? null;
        $this->amount = $attributes['amount'] ?? null;
    }

    public static function all(): Collection
    {
        $dataSourcePath = Config::get('app.data_source.commissions');

        if (!file_exists($dataSourcePath)) {
            return (new self())->newCollection();
        }

        $data = file_get_contents($dataSourcePath);
        $data = json_decode($data, true);
        // todo: check of data consistency?
        $data = array_map(fn(array $row) => new self($row), $data);

        return (new self())->newCollection($data);
    }

    public function newCollection(array $models = []): Collection
    {
        return new Collection($models);
    }
}
