<?php

namespace Database\Seeders;

use App\Models\Account;
use App\Models\Commission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class CommissionSeeder extends Seeder
{
    /**
     * Run the Commission database seeds as JSON file.
     */
    public function run(): void
    {
        $accounts = Account::all();

        $dataSourcePath = Config::get('app.data_source.commissions');

        unlink($dataSourcePath);

        $commissions = [];

        foreach ($accounts as $account) {
            $commissions = array_merge_recursive(
                $commissions,
                Commission::factory()->count(rand(1, 3))->make(['accountId' => $account->id])->toArray()
            );
        }

        file_put_contents($dataSourcePath, json_encode($commissions, JSON_PRETTY_PRINT));
    }
}
