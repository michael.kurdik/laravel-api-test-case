<?php

namespace Database\Seeders;

use App\Models\Account;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class AccountSeeder extends Seeder
{

    /**
     * Run the User database seeds as JSON file.
     */
    public function run(): void
    {
        $accounts = Account::factory()
            ->count(3)
            ->make()
            ->map(fn($account) => $account->toArray())
            ->toArray();

        $dataSourcePath = Config::get('app.data_source.accounts');

        unlink($dataSourcePath);

        file_put_contents($dataSourcePath, json_encode($accounts, JSON_PRETTY_PRINT));
    }

}
