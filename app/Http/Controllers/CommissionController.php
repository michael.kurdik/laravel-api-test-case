<?php

namespace App\Http\Controllers;

use App\Models\Commission;
use App\Models\Payoff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Validator;

class CommissionController extends Controller
{
    public function available()
    {

        $commissionsAmounts = Commission::all();

        $debt = $commissionsAmounts
            ->filter(fn(Commission $commission) => $commission->amount < 0)
            ->sum('amount');

        $profits = $commissionsAmounts
            ->filter(fn(Commission $commission) => $commission->amount > 0);

        $combinations = [];

        for ($i = 0; $i < $profits->count(); $i++) {

            for ($j = 1; $j <= $profits->count() - $i; $j++) {
                $combination = $profits->slice($i, $j);

                $summary = $combination->sum('amount') + $debt;

                if (($summary) <= 0) {
                    continue;
                }

                $combinations[] = $summary;
            }
        }

        sort($combinations);

        return response()->json($combinations);
    }


    public function withdraw(Request $request)
    {
        $executed = RateLimiter::attempt('withdraw', 1, function () {});
        if (!$executed) {
            return response()->json(['message' => 'Too many requests.'], 404);
        }

        // todo: better validation
        $validator = Validator::make($request->all(), [
            'accountId' => 'required|int',
            'commissionId' => 'required|int',
            'amount' => 'required|numeric|min:0',
            'bankAccountNumber' => 'required|string',
        ]);

        // todo: better messages
        if ($validator->fails()) {
            return response()->json(['message' => 'Incorrect incoming parameters.'], 404);
        }

        $accountId = (int) $request->input('accountId');
        $commissionId = (int) $request->input('commissionId');
        $amount = (float) $request->input('amount');
        $bankAccountNumber = $request->input('bankAccountNumber');

        // todo: summary and check if withdraw is possible

        $filteredPayoffs = Payoff::all()->filter(fn(Payoff $payoff) =>
            $payoff->accountId === $accountId && $payoff->commissionId === $commissionId
        );

        if ($filteredPayoffs->count() > 0) {
            return response()->json(['message' => 'Commission has been already processed.'], 404);
        }

        $commissions = Commission::all()->filter(fn(Commission $commission) =>
            $commission->id === $commissionId && $commission->accountId === $accountId && $commission->amount === $amount
        );

        if ($commissions->count() === 0) {
            return response()->json(['message' => 'Unknown withdraw request.'], 404);
        }

        $payoffs = Payoff::all();
        $payoff = new Payoff();

        $payoff->accountId = $accountId;
        $payoff->commissionId = $commissionId;
        $payoff->amount = $amount;
        $payoff->bankAccountNumber = $bankAccountNumber;

        $payoffs->add($payoff);

        $dataSourcePath = Config::get('app.data_source.payoffs');

        unlink($dataSourcePath);

        file_put_contents($dataSourcePath, json_encode($payoffs, JSON_PRETTY_PRINT));

        return response()->json(['message' => 'Withdraw request created successfully.']);
    }

}
